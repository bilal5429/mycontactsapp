package com.example.mycontactsapp;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

public class Repository {

    Context context;

    public Repository(Context context) {
        this.context = context;

    }

    public MutableLiveData fetchContacts() {
        ContactModel contactModel = new ContactModel(context);
        return contactModel.fetchContacts();
    }

}
