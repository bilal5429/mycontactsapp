package com.example.mycontactsapp;


import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ContactModel {
    Cursor cursor;
    final Context context;
    private final MutableLiveData responseLiveData = new MutableLiveData<>();

    ContactModel(Context context) {
        this.context = context;
    }

    public MutableLiveData fetchContacts() {

        ArrayList<Contact> contactsList = new ArrayList<>();

        try {

            cursor = context.getContentResolver()
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            null, null, null);
            assert cursor != null;
            int Idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract
                    .CommonDataKinds.Phone.DISPLAY_NAME);

            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract
                    .CommonDataKinds.Phone.NUMBER);
//            int photoIdIdx = cursor.getColumnIndex(ContactsContract
//            .CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);
            cursor.moveToFirst();

            Set<String> ids = new HashSet<>();
            do {
                String contactid = cursor.getString(Idx);
                if (!ids.contains(contactid)) {
                    ids.add(contactid);
                    String name = cursor.getString(nameIdx);
                    String phoneNumber = cursor.getString(phoneNumberIdx);
//                    String image = cursor.getString(photoIdIdx);
                    Contact contact1 = new Contact();
                    contact1.setContactName(name);
                    contact1.setPhoneNo(phoneNumber);
                    contactsList.add(contact1);
                }

            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        responseLiveData.setValue(contactsList);
        return responseLiveData;
    }

}
