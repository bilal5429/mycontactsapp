package com.example.mycontactsapp;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ContactsViewModel extends ViewModel {

    public MutableLiveData getContactsData(Context context) {
        Repository repository = new Repository(context);
        return repository.fetchContacts();
    }

}
